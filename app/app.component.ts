import { Component } from '@angular/core';
import { User } from './shared/models/user'
import { UsersService } from "./shared/services/users.service";

import {Observable} from "rxjs/Observable";


@Component({
    selector: "my-app",
    templateUrl: "./app/app.component.html",
    styleUrls: ["./app/app.component.css"]
})
export class AppComponent{
    message = "Hello!";

    asyncUsers: Observable<Array<User>>;
    
    constructor(usersService: UsersService){
        //this.users = usersService.getUsers();
        // usersService.userList.subscribe(
        //     (newUsers)=> this.asyncUsers = newUsers
        // );
        this.asyncUsers = usersService.userList;
    }
    activeUser;
    selectUser(user: User){
        this.activeUser = user;
        //console.log(this.activeUser);
    }
}
