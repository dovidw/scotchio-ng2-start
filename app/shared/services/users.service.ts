import { Injectable } from '@angular/core';
import { User } from '../models/user';
import {Observable} from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/Rx";

@Injectable()
export class UsersService {
    private users : User[] = [
        {id: 3685463, name: "Moishe", username: "mzichmich" },
        {id: 56456, name: "Chaim", username: "moishe" },
        {id: 3685463, name: "Berel", username: "tunafish" }
    ];

    private _users : BehaviorSubject<Array<User>> = new BehaviorSubject(this.users.slice(0));

    public userList: Observable<Array<User>> = this._users.asObservable();

    // getUsers() {
    //     return this.users;
    // }
    addUser(user: User){
        this.users.push(user);
        this._users.next(this.users.slice(0));
    }
}