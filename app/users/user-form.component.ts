import { Component } from '@angular/core';
import { User } from '../shared/models/user';
import { UsersService } from "../shared/services/users.service";

@Component({
    selector: 'user-form',
    styles: [`
        form{
            padding: 10px;
            background: #ecf0f1;
            border-radius: 3px;
            margin-bottom: 30px;
        }
    `],
    templateUrl: "./app/users/user-form.component.html"
})
export class UserFormComponent {
    newUser: User = new User;
    active: boolean = true;
    private _usersService;
    constructor(usersService: UsersService){
        this._usersService = usersService;
    }
    onSubmit(){
        console.log(this.newUser);
        this._usersService.addUser(this.newUser);
        this.resetForm();
    }
    resetForm(){
        this.active = false;
        this.newUser = new User;
        setTimeout(()=>  this.active = true, 0);
    }
}